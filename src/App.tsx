import React from 'react';
import i18next from "i18next";
//@ts-ignore
import  { Scrollbars } from 'react-custom-scrollbars';
export interface IState {
    message: string
}

export default class App extends React.Component<{}, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            message: i18next.t('Pages.home.welcome')
        };
    }

    componentDidMount(){

    }
    componentDidUpdate(){

    }
    componentWillUnmount(){

    }
    changeLang(lang:string):any{
        i18next.changeLanguage(lang).then(() => {
            i18next.options.lng = lang;
            i18next.t('key');
            this.setState({
                message: i18next.t('Pages.home.welcome')
            });
        });

    }
    render(){
        return (
            <div>
                <h1 style={{textAlign: 'center'}} className="App">{this.state.message}</h1>
                <button type={"button"} onClick={()=>{this.changeLang('en')}}>English</button>
                <button type={"button"} onClick={()=>{this.changeLang('fr')}}>French</button>
                <button type={"button"} onClick={()=>{this.changeLang('it')}}>Italian</button>
                <Scrollbars style={{ width: 500, height: 200 }}>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                    <p>Some scrollable content...</p>
                </Scrollbars>
            </div>

        );
    }
};
