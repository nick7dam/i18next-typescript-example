import React from 'react';
import i18next from "i18next";
import { IUser } from 'shared/models/default/userModel';
import UserService from 'shared/services/default/userService';
export interface IState {
    users: IUser[],
}
export interface IProps {
}
export default class UserComponent extends React.Component< IProps, IState> {
    public userService: UserService;
    constructor(props:any){
        super(props);
        this.state = {
            users: [],
        };
        this.userService = new UserService();
    }
    componentDidMount(){
        this.retrieveAllEntities();
    }
    componentDidUpdate(){ }
    componentWillUnmount(){ }

    retrieveAllEntities(){
        this.userService.getRequest().then(response => {
        if(response){
            this.setState({
            users: response.data
            });
        }
        });
    }
    render() {
        return (
            <div></div>
        )
    }
}
