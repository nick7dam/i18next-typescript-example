import { Moment } from 'moment';
var moment = require('moment');

export interface IUser {
    id?: number;
    createdOn?: Moment;
    updatedOn?: Moment;
    version?: number;
}

export class User implements IUser {
constructor(
    public id?: number,
    public createdOn?: Moment,
    public updatedOn?: Moment,
    public version?: number
  ){
  }
};
