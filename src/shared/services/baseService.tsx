import axios from 'axios';
import AppConfig from '../app.config'
export default abstract class BaseService<T> {
    protected url:string;
    private appConfig:AppConfig = new AppConfig();
    protected constructor (url:string){
        this.url = url;
        this.appConfig.initAxiosInterceptors();
    }

    public async getRequest(id?:number):Promise<any>{
        return new Promise<any>( resolve => {
            axios.get(this.url + '/' + (id ? id : '')).then(resp => {
                resolve(resp);
            });
        });

    }

    public async postRequest(entity:T):Promise<any>{
        return new Promise<any>(resolve => {
            axios.post(this.url, entity).then(resp => {
                resolve(resp);
            });
        })

    }

    public async patchRequest(entity:T):Promise<any>{
        return new Promise<any>(resolve => {
            axios.patch(this.url, entity).then(resp => {
                resolve(resp);
            });
        })

    }

    public async putRequest(entity:T):Promise<any>{
        return new Promise<any>(resolve => {
            axios.put(this.url, entity).then(resp => {
                resolve(resp);
            });
        })

    }

    public async deleteRequest(id:number):Promise<any>{
        return new Promise<any>(resolve => {
            axios.delete(this.url).then(resp => {
                resolve(resp);
            });
        })

    }
}

