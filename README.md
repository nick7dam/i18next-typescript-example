## React i18next & typescript example

How to combine & use i18next, with React & TypeScript

Important Files:

1. src/indext.tsx
2. src/App.tsx
3. ./tsconfig.json
4. ./Package.json

### To run the example:
1. npm i
2. npm start
3. your app should be available on http://localhost:3000
